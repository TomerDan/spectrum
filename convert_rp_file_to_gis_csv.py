import pandas as pd
import numpy as np

import pytz



tz_eastern = pytz.timezone('US/Eastern')
tz_brussels = pytz.timezone('Europe/Brussels')


df = pd.read_csv('data_rp.csv',low_memory=False)
df["latitude"] = round((df["lat"] / 100),7)
df["longitude"] = round((df["lon"] / 100),7)
df['Timezone'] = pd.to_datetime(df['Time'].astype(str)) + pd.DateOffset(hours=3)

# create a list of our conditions
conditions = [
    (df['RF'] < -35),
    (df['RF'] >= -35)
    ]

# create a list of the values we want to assign for each condition
values = ['0', '5']

# create a new column and use np.select to assign values to it using our lists as arguments
df['W'] = np.select(conditions, values)

del df["lat"]
del df["lon"]




new_df = df.dropna()

new_df.to_csv(f"data_rp_new.csv",index=False)

print(new_df.to_string())

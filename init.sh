#!/bin/bash
sudo apt-get update
sudo apt-get install rtl-sdr -y
sudo apt-get install git -y 
sudo apt-get install cron
sudo apt-get install python3-pip -y
pip3 install pyrtlsdr
pip3 install numpy
pip3 install pyserial
pip3 install pynmea2
git clone https://gitlab.com/TomerDan/spectrum.git
crontab -l | { cat; echo "@reboot python3 /home/pi/spectrum/gps_and_rf.py"; } | crontab -
sudo reboot

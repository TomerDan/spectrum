'''
    Author: Tomer Daniely
    Purpose: Continually Stream GPS data from the NEO-6m GPS
'''

import serial
import pynmea2
from rtlsdr import RtlSdr
import numpy as np
import os
sdr = RtlSdr()

def port_setup(port):
    ser = serial.Serial(port, baudrate=9600, timeout=2)
    return ser

def parseGPSdata(ser):
        keywords = ["$GPRMC","$GPGGA"]
        gps_data = ser.readline()
        gps_data = gps_data.decode("utf-8")  # transform data into plain string

        if len(gps_data) > 5:  # Check to see if the GPS gave any useful data
            if gps_data[0:6] in keywords:   # Check t see if the message code
                gps_msg = pynmea2.parse(gps_data)
                lat = gps_msg.latitude
                lng = gps_msg.longitude
                return (lat,lng)
            else:
                return None
        else:
            return None

if __name__ == "__main__":

    # access serial port
    gps_port = "/dev/ttyUSB0"
    ser = port_setup(gps_port)
    # configure device
    sdr.sample_rate = 2.048e6  # Hz
    sdr.center_freq = 1730e6     # Hz
    sdr.freq_correction = 60   # PPM
    sdr.gain = 10000
    i = 0
    while True:
        path = (f"/home/pi/data_{i}.csv")
        if (os.path.exists(path)):
            i = i + 1
        else:
            break
    
    # Print out GPS cordinates
    print("GPS coordinate Stream:")
    while True:
        sample = sdr.read_samples(128*4096)
        sample = np.real(sample)
        sample = np.max(sample)
        sample = np.around(sample,3)
        f = open(path,"a")
        try:
            gps_coords = parseGPSdata(ser)
            if gps_coords is None:  # if no valid data was received
                f.write("No Data\n")
                f.close()
                print("no data\n")
            else:

                f.write(f"latitude: {gps_coords[0]}, longitude: {gps_coords[1]} ,RF: {sample}\n")
                f.close()
                print(f"lat: {gps_coords[0]}\n")
        except serial.SerialException as e:  # catch any serial communication errors
            print(f"\nERROR: {e}")
            print("... reconnecting to serial\n")
            ser = port_setup()

        except KeyboardInterrupt as e:  # Catch when user hits Ctrl-C and end program
            print("--- Program shutting down ---")
            break


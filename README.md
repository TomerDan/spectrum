How to Run a Program at Startup
There are several ways to run a program at startup on Raspberry Pi. For this guide, though, you'll learn how to use three of these effective and easy to follow methods.

Note: We've used these methods to execute our Python script, and you can do it with any of your scripts or even the onboard programs on Raspberry Pi. Just make sure you're using the right method to execute your program.

1. Use rc.local File
rc.local is a system-administered file that executes after all the system services start, i.e., after switching to a multi-user run level. It is the easiest method to make programs run at boot on Linux systems. But there's a caveat: you can only use this method for programs with no GUI (graphical user interface) elements since rc.local executes before Raspberry Pi's windowing system starts.

To set a program to run at boot, we need to alter the rc.local file and add commands to it. Here's how to do that.

1. Open the terminal and type the following command to open the rc.local file: sudo nano /etc/rc.local.
2. In the rc.local file, enter the following line of code before the "exit 0" line: 

pip3 install pyrtlsdr
pip3 install numpy
pip3 install pyserial
pip3 install pynmea2

python3 gps_spectrum.py

exit 0

3. Here, replace PiCounter/display.py with your program/script name. Also, make sure that you use the absolute path to your program and not its relative path.

After that, hit CTRL + O to save the file.
In the terminal, enter sudo reboot.
(Notice that the command ends with the ampersand (&) symbol. This to inform the system that the program we're scheduling runs continuously, so it shouldn't wait for your script to finish before starting the boot sequence. Do note that failing to add ampersand in the command will cause the script to run forever, and your Pi will never boot up.)

Once your Pi boots up, it should run your program automatically. If, for some reason, you want to stop the program from running on boot, edit the rc.local file again to remove the line you just added.

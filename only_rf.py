from rtlsdr import RtlSdr
import os
import numpy as np
import time

def create_file():
    i = 0
    while True:
        path = (f"/home/pi/data_{i}.csv")
        if (os.path.exists(path)):
            i = i + 1
        else:
            break
    return path
    
def reconnect_rtlsdr():
    # configure device
    while True:
        try:
            sdr = RtlSdr()
        except:
            print("device is not conected\n")
            f = open(path, "a")
            f.write("device is not connected\n")
            f.close()
            time.sleep(5)
        else:
            print("connecting to RtlSdr 1\n")
            break
                
    sdr.sample_rate = 1e6#2.048e6  # Hz
    sdr.center_freq = 1575e6     # Hz
    sdr.freq_correction = 1  # PPM
    sdr.gain = 30
    # Print out spectrum 
    print("spectrum Stream:")
    return sdr












if __name__ == "__main__":
    path = create_file()
    # configure device
    sdr = reconnect_rtlsdr()
    while True:
        while True:
            try:
                sample = sdr.read_samples(128*4096)
            except: 
                print("device is disconnected\n")
                f = open(path, "a")
                f.write("device is disconnected\n")
                f.close()
                time.sleep(5)
                sdr = reconnect_rtlsdr()
            else:
                print("connecting to RtlSdr 2\n")
                break
                
        sample = sample - np.mean(sample)
        sample = np.abs(sample)
        sample = np.max(sample)
        sample = np.around(sample,3)
        sample = 20.0 * np.log10(sample)

        if sample is None:  # if no valid data was received
            print("No signal")
            f = open(path, "a")
            f.write("No signal\n")
            f.close()
        else :
            print(np.around(sample,3))
            f = open(path,"a")
            f.write(str(np.around(sample,3)))











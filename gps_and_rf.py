import serial
import pynmea2
import os
from rtlsdr import RtlSdr
import numpy as np
import time


def port_setup(port):
    while True:
        try:
            ser = serial.Serial(port, baudrate=9600, timeout=0)
            print("serial connected")            
            l = open(LOG_file, "a")
            l.write("LOG: serial is connected\n")
            l.close()
        except:
            l = open(LOG_file, "a")
            l.write("LOG: serial is not connected\n")
            l.close()
            print("serial not  connected")
        else:
            return ser





def parseGPSdata(ser):
    while True:
        try:
            keywords = ["$GPRMC"]
            gps_data = ser.readline()
            dataout = pynmea2.NMEAStreamReader()
            gps_data = gps_data.decode("utf-8")  # transform data into plain string
            if len(gps_data) > 20:  # Check to see if the GPS gave any useful data
                while True:
                    if gps_data[0:6] in keywords:  # Check t see if the message code
                        gps_msg = pynmea2.parse(gps_data)
                        lat = gps_msg.latitude
                        lng = gps_msg.longitude
                        times =gps_msg.timestamp
                        break
                    else:
                        gps_data = ser.readline()
                        gps_data = gps_data.decode("utf-8")
                return (str(lat), str(lng), str(times))
            else:
                l = open(LOG_file, "a")
                l.write("LOG: gps_data<5\n")
                l.close()
                print("LOG: gps_data<5\n")
                return None
        except Exception as e:
            print("LOG: parseGPSdata ERROR\n")
            l = open(LOG_file, "a")
            l.write("LOG: parseGPSdata ERROR\n")
            l.write(f"{e}")
            l.close()


def create_LOG_file():
    i = 0
    while True:
        log_path = (f"/home/pi/LOG_{i}.csv")
        if (os.path.exists(log_path)):
            i = i + 1
        else:
            break
    return log_path


def create_file():
    i = 0
    while True:
        path = (f"/home/pi/data_{i}.csv")
        if (os.path.exists(path)):
            i = i + 1
        else:
            break
    return path


def reconnect_rtlsdr():
    # configure device
    while True:
        try:
            sdr = RtlSdr()            
            print("LOG: RF device conected\n")
            l = open(LOG_file, "a")
            l.write("LOG: RF device connected\n")
            l.close()

        except:
            print("LOG: RF device is not conected\n")
            l = open(LOG_file, "a")
            l.write("LOG: RF device is not connected\n")
            l.close()
            time.sleep(1)
        else:
            print("connecting to RtlSdr 1\n")
            break

    sdr.sample_rate = 1e6  # 2.048e6  # Hz
    sdr.center_freq = 1600e6  # Hz
    sdr.freq_correction = 1  # PPM
    sdr.gain = 30
    # Print out spectrum
    print("spectrum Stream:")
    return sdr


if __name__ == "__main__":
    # create new file
    path = create_file()
    # create log file
    LOG_file = create_LOG_file()

    # access serial port
    gps_port = "/dev/ttyAMA0"
    ser = port_setup(gps_port)
    # configure device
    sdr = reconnect_rtlsdr()
    # header
    f = open(path, "a")
    f.write("RF,lat,lng,time\n")
    f.close()
    # start message in log file
    l = open(LOG_file, "a")
    l.write("satart debug\n")
    l.close()

    # Print out GPS cordinates
    print("GPS coordinate Stream:")
    while True:
        while True:
            try:
                sample = sdr.read_samples(512 * 1024)
            except:
                print("LOG: RF device can't read from sdr obj\n")
                l = open(LOG_file, "a")
                l.write("LOG: RF device can't read from sdr obj\n")
                l.close()
                time.sleep(5)
                sdr = reconnect_rtlsdr()
            else:
                break

        sample = sample - np.mean(sample)
        sample = np.abs(sample)
        sample = np.max(sample)
        sample = np.around(sample, 3)
        sample = 20.0 * np.log10(sample)

        if sample is None:  # if no valid data was received
            print("LOG: No sample")
            f = open(path, "a")
            f.write("LOG: No sample")

        else:
            print(np.around(sample, 3))
            f = open(path, "a")
            f.write(str(np.around(sample, 3)) + ",")

        try:
            gps_coords = parseGPSdata(ser)
        except serial.SerialException as e:  # catch any serial communication errors
            print(f"\n LOG: ERROR: {e}")
            print("LOG: reconnecting to serial -> GPS\n")
            ser = port_setup()

        except KeyboardInterrupt as e:  # Catch when user hits Ctrl-C and end program
            print("LOG: --- Program shutting down ---")
            break

        else:
            if gps_coords is None:  # if no valid data was received
                print("No Data\n")
                l = open(LOG_file, "a")
                l.write("No Data\n")
                l.close()
            else:
                print(f" lat {gps_coords[0]}, lng: {gps_coords[1]} , time: {gps_coords[2]}\n")
                f = open(path, "a")
                f.write(f"{gps_coords[0]}, {gps_coords[1]} ,{gps_coords[2]}\n")
                f.close()


